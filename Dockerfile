## Dockerfile
FROM centos:7

LABEL maintainer "wangl1@bsoft.com.cn"

ENV FASTDFS_PATH=/opt/fdfs \
  FASTDFS_BASE_PATH=/var/fdfs \
  LIBFASTCOMMON_VERSION="1.0.43" \
  FASTDFS_NGINX_MODULE_VERSION="1.22" \
  FASTDFS_VERSION="6.06" \
  NGINX_VERSION="1.17.10" \
  TENGINE_VERSION="2.3.2" \
  PORT= \
  GROUP_NAME= \
  TRACKER_SERVER=

#get all the dependences and nginx
RUN yum install -y git gcc make wget pcre pcre-devel openssl openssl-devel gd-devel\
  && rm -rf /var/cache/yum/*

#create the dirs to store the files downloaded from internet
RUN mkdir ${FASTDFS_BASE_PATH} \
  && mkdir /nginx_conf && mkdir -p /usr/local/nginx/conf/conf.d

COPY fdfs/*.* ${FASTDFS_PATH}/

WORKDIR ${FASTDFS_PATH}

## compile the libfastcommon
RUN tar -zxf libfastcommon-${LIBFASTCOMMON_VERSION}.tar.gz \
  && cd libfastcommon-${LIBFASTCOMMON_VERSION} \
  && ./make.sh \
  && ./make.sh install \
  && rm -rf ${FASTDFS_PATH}/libfastcommon-${LIBFASTCOMMON_VERSION}

## compile the fastdfs
RUN tar -zxf fastdfs-${FASTDFS_VERSION}.tar.gz \
  && cd fastdfs-${FASTDFS_VERSION} \
  && ./make.sh \
  && ./make.sh install \
  && rm -rf ${FASTDFS_PATH}/fastdfs-${FASTDFS_VERSION}

## comile nginx
# nginx url: https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
# tengine url: http://tengine.taobao.org/download/tengine-${TENGINE_VERSION}.tar.gz
RUN tar -zxf fastdfs-nginx-module-${FASTDFS_NGINX_MODULE_VERSION}.tar.gz \
  && wget http://tengine.taobao.org/download/tengine-${TENGINE_VERSION}.tar.gz \
  && tar -zxf tengine-${TENGINE_VERSION}.tar.gz \
  && cd tengine-${TENGINE_VERSION} \
  && ./configure --prefix=/usr/local/nginx --with-http_ssl_module --with-http_image_filter_module --add-module=${FASTDFS_PATH}/fastdfs-nginx-module-${FASTDFS_NGINX_MODULE_VERSION}/src/ \ 
  && make \
  && make install \
  && ln -s /usr/local/nginx/sbin/nginx /usr/bin/ \
  && rm -rf ${FASTDFS_PATH}/fastdfs-nginx-module-${FASTDFS_NGINX_MODULE_VERSION} 

EXPOSE 22122 23000 8088 8888 
VOLUME ["$FASTDFS_BASE_PATH","/etc/fdfs","/usr/local/nginx/conf/conf.d"]   

COPY conf/*.* /etc/fdfs/
COPY nginx_conf/ /nginx_conf/
COPY nginx_conf/nginx.conf /usr/local/nginx/conf/

COPY start.sh /usr/bin/

#make the start.sh executable 
RUN chmod a+x /usr/bin/start.sh

WORKDIR ${FASTDFS_PATH}

ENTRYPOINT ["/usr/bin/start.sh"]
CMD ["tracker"]
