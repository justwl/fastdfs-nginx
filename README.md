# fastdfs-nginx

#### 介绍

本项目中FastDFS整合了Nginx，并在nginx中配置了http_image_filter_module，可以自动生成缩略图。

#### 软件架构
```
fastdfs-nginx
├── conf                        	//配置文件
		├── client.conf
├── http.conf
		├── mime.types
		├── mod_fastdfs.conf
		├── storage.conf
		├── storage_ids.conf
		├── tracker.conf
├── fdfs 							//fastdfs安装包
    ├── fastdfs-6.06.tar.gz			
    ├── fastdfs-nginx-module-1.22.tar.gz	
    ├── libfastcommon-1.0.43.tar.gz	
├── nginx_conf 						//Nginx配置文件
    ├── conf.d			
        ├── fdfs.conf			
        ├── storage.conf			
        ├── tracker.conf		
    ├── nginx.conf		
├── docker-compose.yml				
├── Dockerfile				
├── LICENSE					
├── README.md
```

#### 安装
下载fastdfs-nginx项目，进入项目目录，执行以下命令：
```
docker build -t wl/fastdfs-nginx . 
```
#### 启动
```
docker network create fastdfs-net

docker run -dit --network=fastdfs-net --name tracker -v /var/fdfs/tracker:/var/fdfs wl/fastdfs-nginx:latest tracker

docker run -dit --network=fastdfs-net --name storage0 -e TRACKER_SERVER=tracker:22122 -v /var/fdfs/storage0:/var/fdfs wl/fastdfs-nginx:latest storage

docker run -dit --network=fastdfs-net --name storage1 -e TRACKER_SERVER=tracker:22122 -v /var/fdfs/storage1:/var/fdfs wl/fastdfs-nginx:latest storage
```
